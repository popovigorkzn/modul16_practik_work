﻿#include <iostream>
#include <time.h>
using namespace std;

const int STR = 5; //кол-во строк массива 
const int COL = 5; //кол-во колонок массива 

int main()
{
	struct tm buf;			//
	time_t t = time(NULL);	//	 возвращает
	localtime_s(&buf, &t);	//  сегодняшнюю 
	buf.tm_mday;			//     дату tm_mday

	int index = buf.tm_mday % STR; // индекс остатка деления текущего чилса каленжаря (buf.tm_mday) на N (COL = 5)

	int arr[STR][COL]{}; 
	int sum = 0;			// {} заполнение массива нулями, контейнер суммы массива sum 

	for (int i = 0; i < STR; i++) 
	{
		for (int j = 0; j < COL; j++)
		{
			arr[i][j] = i + j; // заполнение массива элементами 
			cout << arr[i][j] << " ";
		}
		
		
	}

	for (int j = 0; j < COL; j++)
	{
		sum += arr[index][j];	// сумма элемениов определенной строки массива 
	}

	cout << "On " << buf.tm_mday << " date summa =" << sum;

	return 0;
}